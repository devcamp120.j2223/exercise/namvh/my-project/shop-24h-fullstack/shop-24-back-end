const express = require("express");
const router = express.Router();
const middleWare = require("../middleware/middleware");

const {
  CreateProductType,
  GetAllProductType,
  GetProductTypeByID,
  UpdateProductType,
  DeleteProductType,
} = require("../controllers/productTypeController");

router.use(middleWare);

router.post("/productTypes", CreateProductType);
router.get("/productTypes", GetAllProductType);
router.get("/productTypes/:productId", GetProductTypeByID);
router.put("/productTypes/:productId", UpdateProductType);
router.delete("/productTypes/:productId", DeleteProductType);

module.exports = router;
