const express = require("express");
const router = express.Router();
const middleWare = require("../middleware/middleware");

const {
  createProduct,
  getAllProduct,
  getProductById,
  updateProduct,
  deleteProduct,
} = require("../controllers/productController");

router.use(middleWare);

router.post("/products", createProduct);
router.get("/products", getAllProduct);
router.get("/products/:productId", getProductById);
router.put("/products/:productId", updateProduct);
router.delete("/products/:productId", deleteProduct);

module.exports = router;
