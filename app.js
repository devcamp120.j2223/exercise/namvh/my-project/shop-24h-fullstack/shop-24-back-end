const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();
app.use(morgan("combined"));
app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
const port = 8000;

const productTypeRouter = require("./app/routers/productTypeRouter");
const productRouter = require("./app/routers/productRouter");
const customerRouter = require("./app/routers/customerRouter");
const orderRouter = require("./app/routers/orderRouter");

var url = "mongodb://localhost:27017/Shop24h_Project";

mongoose.connect(url, (err) => {
  if (err) {
    console.log("Unable to connect to the mongoDB server. Error:", err);
  } else console.log(`Connecting established to`, url);
});

app.use("/", productTypeRouter);
app.use("/", productRouter);
app.use("/", customerRouter);
app.use("/", orderRouter);

app.listen(port, () => {
  console.log(`App is running on port at http://localhost${port}`);
});
